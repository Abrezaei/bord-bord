let width = window.innerWidth;
let request = null;
let mouse = { x: 0, y: 0 };
let cx = window.innerWidth / 2;
let cy = window.innerHeight / 2;
if (width > 768) {

    // Rocket image movement
    gsap.fromTo("#rocket",
        {top: 50},
        {top: 100, duration: 3, ease: "power1.inOut", repeat: -1, yoyo: true, yoyoEase: ""}
    );

    // Cloud images movement
    $('header').mousemove(function(event) {

        mouse.x = event.pageX;
        mouse.y = event.pageY;

        cancelAnimationFrame(request);
        request = requestAnimationFrame(update);
    });

    $(window).resize(function() {
        cx = window.innerWidth / 2;
        cy = window.innerHeight / 2;
    });
} else {
    gsap.fromTo("#rocket",
        {top: 50},
        {top: 75, duration: 3, ease: "power1.inOut", repeat: -1, yoyo: true, yoyoEase: ""}
    );
}

function update() {

    dx = mouse.x - cx;
    dy = mouse.y - cy;

    tilty = -20 * (dy / cy);
    tiltx = -(dx / cx) * 20;
    radius = Math.sqrt(Math.pow(tiltx,2) + Math.pow(tilty,2));
    degree = (radius * 20);
    TweenLite.to(".cloud1", 6, {transform:'translate3d(' + tiltx*3 + 'px, ' + tilty*3 + 'px,' + 0 + 'px)', ease:Power2.easeOut});
    TweenLite.to(".cloud2", 4, {transform:'translate3d(' + tiltx + 'px, ' + tilty + 'px,' + 0 + 'px)', ease:Power2.easeOut});
    TweenLite.to(".cloud3", 2, {transform:'translate3d(' + tiltx/2 + 'px, ' + tilty/2 + 'px,' + 0 + 'px)', ease:Power2.easeOut});
}